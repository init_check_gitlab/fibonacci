#include<iostream>
#include<cassert>

using namespace std;

int fib(int n) {
    int a=0, b=1, c=0;
    for(int i=1; i<=n; i++) {
        c=a+b;
        a=b;
        b=c;
    }
    return a;
}

int main() {
    assert(fib(0) == 0);
    assert(fib(1) == 1);
    assert(fib(2) == 1);
    assert(fib(3) == 2);
    assert(fib(4) == 3);
    assert(fib(5) == 5);

    cout<<"All test cases passed!"<<endl;
    return 0;
}